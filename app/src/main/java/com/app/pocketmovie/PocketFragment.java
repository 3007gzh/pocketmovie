package com.app.pocketmovie;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.pocketmovie.data.FilmDAO;
import com.app.pocketmovie.data.FilmDatabase;
import java.util.ArrayList;
import java.util.List;

//this is the PocketFragment class which acts as an activity where the different film items saved in the database are shown (via a RecyclerView)

public class PocketFragment extends Fragment {

    View p;
    private RecyclerView pocketRecyclerView;
    private PocketAdapter pocketRecyclerAdapter;
    private List<FilmDetailed> filmsDetailed;

    public PocketFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //the onCreateView method is called after the onCreate method; its purpose is to inflate the layout and set the adapter to which the data will be affected
        filmsDetailed = new ArrayList<>();
        p = inflater.inflate(R.layout.fragment_pocket, container, false);
        pocketRecyclerView = p.findViewById(R.id.pocket_recyclerview);
        pocketRecyclerAdapter = new PocketAdapter(getContext(), filmsDetailed);
        pocketRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        pocketRecyclerView.setAdapter(pocketRecyclerAdapter);
        return p;
    }

    @Override
    public void onStart() {
        super.onStart();
        //when the activity is started, the FilmDatabase class is called to build the database
        //all the film items present in the database are retrieved through the "getAllFilmsDAO" method
        //the different film items are then passed onto the adapter to be shown on screen
        FilmDAO filmDAO = FilmDatabase.getInstance(getContext()).filmDAO();
        filmsDetailed = filmDAO.getAllFilmsDAO();
        pocketRecyclerAdapter.setFilmsDetailed(filmsDetailed);
        pocketRecyclerAdapter.notifyDataSetChanged();
    }
}