package com.app.pocketmovie;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

//this class acts as a service which handles the HttpRequests sent from other classes

public class HttpRequestService extends IntentService {

    private OkHttpClient client;
    private static final String EXTRA_URLSTRING = "PocketMovie Extra URL String";

    public HttpRequestService(){
        super("HttpRequestService");
        client = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();
    }

    public static void startActionRequestHttp(Context context, String url) {
        //a new intent is built using the URL information that was passed through
        //it will then be used by the service to build the URL
        Intent intent = new Intent(context, HttpRequestService.class);
        intent.putExtra(EXTRA_URLSTRING, url);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent){
        //the extra data contained in the intent from the startActionRequestHttp method is retrieved here and the request to the API is then launched based on the URL
        //if everything goes as planned, a response string is received from the API (in JSON format) and is then broadcast to be used by another class
        if (intent != null) {
            String url = intent.getStringExtra(EXTRA_URLSTRING);

            try {
                String responseString = startRequest(url);
                Intent completeIntent = new Intent("httpRequestComplete");
                completeIntent.putExtra("responseString", responseString);
                sendBroadcast(completeIntent);
            }

            catch (IOException e) {
            //in case the HttpRequest could not be successful, an error message is raised and shown on screen
                Intent errorIntent = new Intent("httpRequestError");
                sendBroadcast(errorIntent);
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Problem contacting the server. Please retry.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String startRequest(String url) throws IOException {
        //this is the method which sends a request to the API to retrieve data
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}