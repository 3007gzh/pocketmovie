package com.app.pocketmovie;

import android.text.TextUtils;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FilmParser {
    //here we have two different methods, depending on the nature of the object to be parsed
    //one is for the film items in the RecyclerView, with limited data (title, rating, thumbnail)
    //another one is for the selected film item which will be shown in a separate activity (DetailedActivity) with more information to parse

    public static ArrayList<Film> parseFilmResponse(String jsonString) {
        ArrayList<Film> films = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonFilms = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonFilms.length(); ++i) {
                JSONObject jsonFilm = jsonFilms.getJSONObject(i);
                int id = jsonFilm.getInt("id");
                String title = jsonFilm.getString("title");
                double voteAverageDouble = jsonFilm.getDouble("vote_average");
                String voteAverage = Double.toString(voteAverageDouble);
                String posterPath = jsonFilm.getString("poster_path");
                Film newFilm = new Film(id, title, posterPath, voteAverage);
                films.add(newFilm);
            }

        } catch (JSONException e) {
            Log.e("Parsing issue", "Can't parse JSON string correctly");
            e.printStackTrace();
        }

        return films;
    }

    public static ArrayList<FilmDetailed> parseFilmResponseDetailed(String jsonString) {
        ArrayList<FilmDetailed> filmDetailed = new ArrayList<FilmDetailed>();
        ArrayList<String> listGenres = new ArrayList<>();
        ArrayList<String> listYoutubeKeys = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonFilmGenres = jsonObject.getJSONArray("genres");
            JSONObject jsonVideo = jsonObject.getJSONObject("videos");
            JSONArray jsonVideoLinks = jsonVideo.getJSONArray("results");

            int idDetailed = jsonObject.getInt("id");
            String posterPathDetailed = jsonObject.getString("poster_path");
            String originalTitle = jsonObject.getString("title");
            String releaseDate = jsonObject.getString("release_date");
            String backdropPath = jsonObject.getString("backdrop_path");
            String overview = jsonObject.getString("overview");
            double voteAverageDetailedDouble = jsonObject.getDouble("vote_average");
            String voteAverageDetailed = Double.toString(voteAverageDetailedDouble).replace("0.0","n.a.");

            for (int j = 0; j < jsonFilmGenres.length(); ++j) {
                //as there can be many genres, we concatenate them using the TextUtils library
                JSONObject jsonFilmGenre = jsonFilmGenres.getJSONObject(j);
                String genres = jsonFilmGenre.getString("name");
                listGenres.add(genres);
            }
            String genre = TextUtils.join(" - ", listGenres);

            for (int k = 0; k < jsonVideoLinks.length(); ++k) {
                JSONObject jsonVideoDetails = jsonVideoLinks.getJSONObject(k);
                String youtubeKeys = jsonVideoDetails.getString("key");
                listYoutubeKeys.add(youtubeKeys);
            }

            String youtubeKey = null;
            if (listYoutubeKeys.size()>0) {
                youtubeKey = listYoutubeKeys.get(0);
            }

            FilmDetailed filmDetails = new FilmDetailed(idDetailed, originalTitle, releaseDate, genre, overview, backdropPath, voteAverageDetailed, youtubeKey, posterPathDetailed);
            filmDetailed.add(filmDetails);

        } catch (JSONException e) {
            Log.e("Parsing issue", "Can't parse JSON string correctly");
            e.printStackTrace();
        }

        return filmDetailed;
    }
}