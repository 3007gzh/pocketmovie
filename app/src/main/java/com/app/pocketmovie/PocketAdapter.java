package com.app.pocketmovie;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.pocketmovie.data.FilmDAO;
import com.app.pocketmovie.data.FilmDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

//this class defines the different elements that constitute the RecyclerView of the PocketFragment
//here the view model is specified and is used by the adapter in the PocketFragment class to bind data to the user's screen

public class PocketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context ;
    public List<FilmDetailed> filmsDetailed;

    public PocketAdapter(Context context, List<FilmDetailed> filmsDetailed) {
        this.context = context;
        this.filmsDetailed = filmsDetailed;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //the Pocket layout is inflated when the activity starts
        View pocketView;
        pocketView = LayoutInflater.from(context).inflate(R.layout.item_film, viewGroup, false);
        return new PocketViewHolder(pocketView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int i) {
        //the data from the database is fetched and set to the view
        PocketViewHolder pocketViewHolder = (PocketViewHolder) holder;
        FilmDAO filmDAO = FilmDatabase.getInstance(context).filmDAO();
        filmsDetailed = new ArrayList<>(filmDAO.getAllFilmsDAO());
        pocketViewHolder.filmTitle.setText(filmsDetailed.get(i).getOriginalTitle());
        pocketViewHolder.voteAverage.setText("Rating: " + filmsDetailed.get(i).getVoteAverageDetailed());
        Picasso.get().load(TmdbApiURLs.posterPathBaseUrl + filmsDetailed.get(i).getPosterPathDetailed()).into(pocketViewHolder.thumbnail);

        pocketViewHolder.filmClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pocketView) {
                //when the Pocket Movie is clicked, an intent is sent to the DetailedActivity class to start,
                //with the Movie ID as information and an extra string to inform the Activity that the intent comes from the Pocket environment
                FilmDetailed film = filmsDetailed.get(i);
                int idInt = film.getIdDetailed();
                String id = Integer.toString(idInt);
                Intent intent = new Intent(context, DetailedActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, id);
                context.startActivity(intent);
            }
        });
    }

    public void setFilmsDetailed(List<FilmDetailed> filmsDetailed) {
        //the detailed movie is set
        this.filmsDetailed = filmsDetailed;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //we get the size of items to be shown
        return filmsDetailed.size();
    }

    public static class PocketViewHolder extends RecyclerView.ViewHolder {
    //this is the view holder which will hold the different layout elements together

        public TextView filmTitle;
        public TextView voteAverage;
        public ImageView thumbnail;
        public ConstraintLayout filmClick;

        public PocketViewHolder(@NonNull View itemView) {
            super(itemView);

            filmTitle = itemView.findViewById(R.id.film_title);
            voteAverage = itemView.findViewById(R.id.vote_average);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            filmClick = itemView.findViewById(R.id.click_layout);
        }
    }
}