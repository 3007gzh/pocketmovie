package com.app.pocketmovie;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "filmsDetailed")

public class FilmDetailed {
//the "Film Detailed" object (which will be used inside the DetailedActivity class) is defined here by defining the variables that constitute it
//we define getters methods so other classes can call it
//we also attach information to each variable with Room which will be useful for saving movies that have been "pocketed" to the database

    @PrimaryKey(autoGenerate = true)
    //denotes id as the primary key
    private int idDetailed;
    //the following are the different columns which constitute the SQL table
    @ColumnInfo(name = "FilmTitle")
    private String originalTitle;
    @ColumnInfo(name = "ReleaseDate")
    private String releaseDate;
    @ColumnInfo(name = "Genre")
    private String genre;
    @ColumnInfo(name = "Overview")
    private String overview;
    @ColumnInfo(name = "BackdropPath")
    private String backdropPath;
    @ColumnInfo(name = "Rating")
    private String voteAverageDetailed;
    @ColumnInfo(name = "YoutubeKey")
    private String youtubeKey;
    @ColumnInfo(name = "PosterPath")
    private String posterPathDetailed;

    public FilmDetailed(int idDetailed, String originalTitle, String releaseDate, String genre, String overview, String backdropPath, String voteAverageDetailed, String youtubeKey, String posterPathDetailed) {
        //indicates the current instance of the FilmDetailed object inside the class
        this.idDetailed = idDetailed;
        this.originalTitle = originalTitle;
        this.releaseDate = releaseDate;
        this.genre = genre;
        this.overview = overview;
        this.backdropPath = backdropPath;
        this.voteAverageDetailed = voteAverageDetailed;
        this.youtubeKey = youtubeKey;
        this.posterPathDetailed = posterPathDetailed;
    }

    //getter methods to retrieve the movie id, title, release date, genre(s), overview, backdrop path, rating, YouTube key and poster path
    public int getIdDetailed() {
        return idDetailed;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getGenre() {
        return genre;
    }

    public String getOverview() {
        return overview;
    }

    public String getBackdropPath() {
        return TmdbApiURLs.posterPathBaseUrl + backdropPath;
    }

    public String getVoteAverageDetailed() {
        return voteAverageDetailed;
    }

    public String getYoutubeKey() {
        return youtubeKey;
    }

    public String getPosterPathDetailed() {return  posterPathDetailed;}
}