package com.app.pocketmovie.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.app.pocketmovie.FilmDetailed;

//the Room database environment is defined here
//this class serves as the initial builder of the database

@Database(entities = {FilmDetailed.class}, version = 1, exportSchema = false)
public abstract class FilmDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "PocketMovieDB";
    public static FilmDatabase sInstance;

    public static FilmDatabase getInstance(Context context) {
        if (sInstance == null) {

            RoomDatabase.Builder<FilmDatabase> dbBuilder = Room.databaseBuilder(context.getApplicationContext(), FilmDatabase.class, FilmDatabase.DATABASE_NAME);
            dbBuilder.allowMainThreadQueries();
            sInstance = dbBuilder.build();
        }

        return sInstance;
    }

    public abstract FilmDAO filmDAO();
    //an abstract "filmDAO" method is defined which links us with the FilmDAO interface
}