package com.app.pocketmovie.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RoomWarnings;

import com.app.pocketmovie.FilmDetailed;

import java.util.List;

//here we create the DAO interface which will be used to query the database

@Dao
public interface FilmDAO {

    //an "Insert" method is defined; when called, it will insert the entry with all the corresponding data into the database
    //if the entry already exists, it is replaced
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFilm(FilmDetailed insertedFilmDetailed);

    //this query lets us select all the film items in the database with the necessary information to load each film as film items in the Pocket's tab RecyclerView
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT idDetailed, FilmTitle, PosterPath, Rating FROM filmsDetailed order by Rating DESC")
    List<FilmDetailed> getAllFilmsDAO();

    //this method is used to select all the ids which are already present in the database
    @Query("SELECT idDetailed FROM filmsDetailed")
    List<Integer> getIdDetailed();

    //a "Delete" method is defined; when called, it will delete the entry from the database
    //as a result, it will be removed from the Pocket tab as well
    @Delete
    void deleteFilm(FilmDetailed deletedFilmDetailed);
}