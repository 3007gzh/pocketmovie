package com.app.pocketmovie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.pocketmovie.data.FilmDAO;
import com.app.pocketmovie.data.FilmDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailedActivity extends AppCompatActivity {

    public ArrayList<FilmDetailed> filmDetailed;
    public static FilmDetailed selectedFilm;
    public HttpReceiver httpReceiver = new HttpReceiver();
    public static IntentFilter intentFilter = new IntentFilter();
    public static List<Integer> pocketIds;

    //the HttpReceiver will receive information that results from the http request and which will help us build the object with data coming from the API
    private class HttpReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            //in case the http request is successful, we are able to send the data for parsing and populate the different layout elements with the corresponding information
            if (intent.getAction().equals("httpRequestComplete")) {

                String response = intent.getStringExtra("responseString");

                //the following actions are done inside the OnReceive method because it is the last method called inside the activity,
                //therefore here we can use the data we have received and the variables we have previously defined

                //parse the response string into a FilmDetailed object
                filmDetailed = FilmParser.parseFilmResponseDetailed(response);
                setFilmDetailed(filmDetailed);

                //load the user interface
                loadUI();

                //unregister the HttpReceiver to avoid conflicts
                unregisterReceiver(httpReceiver);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        intentFilter.addAction("httpRequestComplete");
        registerReceiver(httpReceiver, intentFilter);

        //get the intent that started this activity
        Intent intent = getIntent();
        if (intent.hasExtra(Intent.EXTRA_TEXT)) {

            //extract the movie id from the intent
            String id = intent.getStringExtra(Intent.EXTRA_TEXT);

            //build the URL with the data from the intent
            String url = TmdbApiURLs.baseUrlDetailed + id + TmdbApiURLs.tailUrlDetailed;

            //send a HttpRequest query
            Context applicationContext = getApplicationContext();
            HttpRequestService.startActionRequestHttp(applicationContext, url);

            Integer idInt = Integer.valueOf(id);
            //check if the id of the movie is present in the Pocket database; if it is the case, the checkbox is ticked, otherwise it is not ticked
            FilmDAO filmDAO = FilmDatabase.getInstance(getApplicationContext()).filmDAO();
            pocketIds = filmDAO.getIdDetailed();
            if (pocketIds.contains(idInt)) {
                CheckBox checkBox = findViewById(R.id.checkbox_pocket);
                checkBox.setChecked(true);
            } else {
                CheckBox checkBox = findViewById(R.id.checkbox_pocket);
                checkBox.setChecked(false);
            }
        }
    }

    public void setFilmDetailed(ArrayList<FilmDetailed> filmDetailed) {
        //setter for the filmDetailed object
        if (filmDetailed != null)
            DetailedActivity.selectedFilm = filmDetailed.get(0);
    }

    public void loadUI() {
        //attach data to each layout element which will show on the user's screen
        TextView originalTitle = findViewById(R.id.film_title);
        originalTitle.setText(selectedFilm.getOriginalTitle());

        TextView releaseDate = findViewById(R.id.release_date);
        releaseDate.setText(getString(R.string.release_date_string, selectedFilm.getReleaseDate()));

        TextView genre = findViewById(R.id.film_genre);
        genre.setText(selectedFilm.getGenre());

        TextView voteAverage = findViewById(R.id.vote_average);
        voteAverage.setText(getString(R.string.vote_average_string, selectedFilm.getVoteAverageDetailed()));

        TextView overview = findViewById(R.id.film_overview);
        overview.setText(selectedFilm.getOverview());

        ImageView backdropPath = findViewById(R.id.backdrop_path);
        Picasso.get().load(selectedFilm.getBackdropPath()).into(backdropPath);

        Button buttonVideo = findViewById(R.id.button_video);
        buttonVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //a click listener is set on the button to send a new implicit intent to open the trailer of the movie on YouTube (with the youtube key attached to the intent)
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + selectedFilm.getYoutubeKey()));
                startActivity(intent);
            }
        });

        final CheckBox checkBox = findViewById(R.id.checkbox_pocket);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            //a checkbox listener is set so when the user checks the box, a new entry is made in the database to save the movie's data which will show inside the Pocket tab
            //if the checkbox is unchecked by the user, the "delete" method from our DAO interface is called and the movie is consequently removed from the database (and therefore from the Pocket tab too)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //if the box is checked by the user, insert into the database
                    FilmDetailed insertedFilmDetailed = selectedFilm;
                    FilmDAO filmDAO = FilmDatabase.getInstance(getApplicationContext()).filmDAO();
                    filmDAO.insertFilm(insertedFilmDetailed);

                    //if the box is unchecked by the user, remove from the database
                } else {
                    FilmDetailed deletedFilmDetailed = selectedFilm;
                    FilmDAO filmDAO = FilmDatabase.getInstance(getApplicationContext()).filmDAO();
                    filmDAO.deleteFilm(deletedFilmDetailed);
                }
            }
        });
    }
}