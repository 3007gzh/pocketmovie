package com.app.pocketmovie;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

//this class defines the different elements that constitute the RecyclerView of the RecentFragment
//here the view model is specified and is used by the adapter in the RecentFragment class to bind data to the user's screen

public class RecentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public List<Film> films;
    private final int VIEW_NORMAL = 0;
    private final int VIEW_FOOTER = 1;
    public static int page = 1;

    public RecentAdapter(Context context, List<Film> films) {
        this.context = context;
        this.films = films;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        //here we have defined two views:
        //one is the "normal view" which is defined by the "item_film" layout
        //the other one is the "footer view" which is defined by the "button_next" layout
        if (viewType == VIEW_NORMAL) {
            v = LayoutInflater.from(context).inflate(R.layout.item_film, parent, false);
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.button_next, parent, false);
        } return new ItemViewHolder(v); }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        //once we reach the end of the films' list, it means we have to add the footer (the "Next Page" button) at the end
        //a click listener is set on the button; when clicked, a new HttpRequest is sent to retrieve data from the API which will then be shown on screen when the user wants to load more movies
        //since the data is divided in pages containing 20 movies each, we have to increment the page number each time the user clicks on the button, to be able to load the following page
        if (position == films.size()) {
            itemViewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    page++;
                    String pageNr = Integer.toString(page);
                    String url = TmdbApiURLs.apiUrl + pageNr;
                    HttpRequestService.startActionRequestHttp(context.getApplicationContext(), url);
                }
            });
        }
        //the "normal" view, holding the film item elements, is defined here
        //we also add a click listener on each film item; when clicked, an intent is sent to the DetailedActivity class to start a new activity with the movie ID passed through EXTRA_TEXT
        else {
            itemViewHolder.filmTitle.setText(films.get(position).getTitle());
            itemViewHolder.voteAverage.setText("Rating: " + films.get(position).getVoteAverage().replace("0.0","n.a."));
            Picasso.get().load(films.get(position).getPosterPath()).into(itemViewHolder.thumbnail);
            itemViewHolder.filmClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Film film = films.get(position);
                    int idInt = film.getId();
                    String id = Integer.toString(idInt);
                    Intent intent = new Intent(context, DetailedActivity.class);
                    intent.putExtra(Intent.EXTRA_TEXT, id);
                    context.startActivity(intent);
                }
            });}
        }

    public void setFilms(List<Film> films) {
        this.films = films;
    }
    //setter for the list of films

    @Override
    public int getItemViewType(int position) {
        //here we define that we have two different view types and at which position each one has to be shown
        //in this case, it will always show the normal view unless the end of the films' list is reach; if this is the case, then the footer view is loaded
        return (position == films.size()) ? VIEW_FOOTER : VIEW_NORMAL;
    }

    @Override
    public int getItemCount() {
        return films.size() + 1;
    }
    //we define the number of elements to show: the list of films as well as one more element which is the "Next Page" button

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
    //the characteristics of the view holder are defined here

        private TextView filmTitle;
        private TextView voteAverage;
        private ImageView thumbnail;
        private Button button;
        private ConstraintLayout filmClick;

        private ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            filmTitle = itemView.findViewById(R.id.film_title);
            voteAverage = itemView.findViewById(R.id.vote_average);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            button = itemView.findViewById(R.id.button_next);
            filmClick = itemView.findViewById(R.id.click_layout);
        }
    }
}