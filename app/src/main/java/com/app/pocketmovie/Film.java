package com.app.pocketmovie;

import android.os.Parcel;
import android.os.Parcelable;

//this class defines our Film object (the "film item" shown inside the two RecyclerViews
//this object is made parcelable to be able to save it inside a bundle in order to reload its state if the activity where the object was initialized is stopped/destroyed

public class Film implements Parcelable {

    protected Film(Parcel in) {
        //first we need to read the data in each variable to start the "parcelization" process
        id = in.readInt();
        title = in.readString();
        posterPath = in.readString();
        voteAverage = in.readString();
    }

    public static final Creator<Film> CREATOR = new Creator<Film>() {
        //then we need to define a Creator type for the object
        @Override
        public Film createFromParcel(Parcel in) {
            return new Film(in);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };

    @Override
    //obligatory method to be defined
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //the data for each object is then written for later usage
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(posterPath);
        dest.writeString(voteAverage);
    }

    private int id;
    private String title;
    private String posterPath;
    private String voteAverage;

    public Film(int id, String title, String posterPath, String voteAverage) {
        //indicates the current instance of the Film object inside the class
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.voteAverage = voteAverage;
    }

    //getter methods to retrieve the movie id, title, poster path and rating
    public int getId() { return id; }

    public String getTitle() {
        return title;
    }

    public String getPosterPath() {
        return TmdbApiURLs.posterPathBaseUrl + posterPath;
    }

    public String getVoteAverage() {
        return voteAverage;
    }
}