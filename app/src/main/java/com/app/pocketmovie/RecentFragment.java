package com.app.pocketmovie;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

//this is the RecentFragment class which acts as an activity where the different film items are shown (via a RecyclerView)

public class RecentFragment extends Fragment implements FilmRepository.DataUIListener {

    View v;
    private RecyclerView recentRecyclerView;
    private RecentAdapter recentRecentAdapter;
    public ArrayList<Film> films;

    public RecentFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //the onCreateView method is called after the onCreate method; its purpose is to inflate the layout and set the adapter to which the data will be affected
        films = new ArrayList<>();
        v = inflater.inflate(R.layout.fragment_recent, container, false);
        recentRecyclerView = v.findViewById(R.id.recent_recyclerview);
        recentRecentAdapter = new RecentAdapter(getContext(), films);
        recentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recentRecyclerView.setAdapter(recentRecentAdapter);
        setRetainInstance(true);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //when the activity is created, we check if the list of films is null in order to load initial data
        if (films == null) {
            FilmRepository.getInstance().init(getContext());
            FilmRepository.getInstance().fetchFilms(getContext());
            FilmRepository.getInstance().observe(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //when the activity is relaunched after it was stopped/destroyed, the bundle that was saved is reloaded so the user can resume the activity where he left it
        if (savedInstanceState != null) {
            films = savedInstanceState.getParcelableArrayList("films");
            recentRecentAdapter.setFilms(films);
        }
    }

    public void updateUI() {
        //this method is called when the UI has to be updated because of any change that is made to the data set
        //in this case, the method is called when new data has been treated and parsed through the FilmRepository class
        //here, this happens when the user requests more data to be loaded by clicking on the "Next Page..." button
        //the new data is added to the existing list of films and the RecyclerView adapter is notified of the change and the list of films is updated on the UI
        films.addAll(FilmRepository.getInstance().getFilms());
        recentRecentAdapter.setFilms(films);
        recentRecentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //when the activity is stopped/destroyed, the film objects (which were made parcelable) are kept in memory through the creation of a bundle in the form of an array containing all the parcelable film objects
        //the bundle is then reloaded when the OnActivityCreated method is called (when the activity is restarted)
        outState.putParcelableArrayList("films", films);
    }
}