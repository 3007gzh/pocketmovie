package com.app.pocketmovie;

//here we have hardcoded some String variables, mainly used by other classes in the application to build URLs

public class TmdbApiURLs {
    public static String posterPathBaseUrl = "https://image.tmdb.org/t/p/w780";
    public static String apiUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key=ea2dcee690e0af8bb04f37aa35b75075&language=en-US&page=";
    public static String firstApiUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key=ea2dcee690e0af8bb04f37aa35b75075&language=en-US&page=1";
    public static String baseUrlDetailed = "https://api.themoviedb.org/3/movie/";
    public static String tailUrlDetailed = "?api_key=ea2dcee690e0af8bb04f37aa35b75075&append_to_response=videos";
}