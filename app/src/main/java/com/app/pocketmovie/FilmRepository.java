package com.app.pocketmovie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

    public class FilmRepository {

        //we initialize a DataUIListener interface with an "updateUI" method to be used by the adapter in our RecentFragment class when there are new films to be loaded on screen
        public interface DataUIListener {
            void updateUI();
        }

        private WeakReference<DataUIListener> dataUIListener;

        //we initialize a singleton to ensure that there is only one instance of the class created
        private static final FilmRepository ourInstance = new FilmRepository();

        public static FilmRepository getInstance() {
            return ourInstance;
        }

        private static ArrayList<Film> films;

        private FilmRepository() {
        }

        //we need a HttpReceiver class in order to be able to handle the response strings received after a HttpRequest was made
        private class HttpReceiver extends BroadcastReceiver {

            @Override
            public void onReceive(Context context, Intent intent) {
                //if everything works as intended, the HttpRequest is successful and the response string from the API in JSON format is then parsed
                if (intent.getAction().equals("httpRequestComplete")) {
                    String response = intent.getStringExtra("responseString");
                    films = FilmParser.parseFilmResponse(response);
                }
                //if the HttpRequest encountered an error, a text is shown informing the user that something went wrong
                else if (intent.getAction().equals("httpRequestError")){
                    Log.d("HttpRequest Error", "HTTP issue: could not fetch data from the server");
                    Toast.makeText(context, "Problem contacting the server. Please retry.", Toast.LENGTH_SHORT).show();
                }
                //if new data is ready to be shown on user's screen, the updateUI() method is called
                if (dataUIListener.get() != null) {
                    dataUIListener.get().updateUI();
                }
            }
        }

        public void init(Context context) {
            //register the broadcast receiver to intent with action "httpRequestComplete" and "httpRequestError"
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("httpRequestComplete");
            intentFilter.addAction("httpRequestError");
            FilmRepository.HttpReceiver httpReceiver = new FilmRepository.HttpReceiver();
            context.getApplicationContext().registerReceiver(httpReceiver, intentFilter);
        }

        public void fetchFilms(Context context) {
            //when starting the application, the RecentFragment onCreate() method checks if the list of films is null
            //if the list is null, then a first set of films is fetched from the database
            if (films == null) {
                String url = TmdbApiURLs.firstApiUrl;
                HttpRequestService.startActionRequestHttp(context.getApplicationContext(), url);
            }
        }

        public void observe(DataUIListener dataUIListener) {
            //here we make a reference to our DataUIListener
            this.dataUIListener = new WeakReference<>(dataUIListener);
        }

        public ArrayList<Film> getFilms() { return films; }
        //getter method to retrieve the films which have been treated/parsed
    }